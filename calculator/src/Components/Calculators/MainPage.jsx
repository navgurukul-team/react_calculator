import React, { useState } from 'react'
import "./Main.css"

function MainPage() {
  const [getValue, setValue] = useState('');
  const [result, setResult] = useState('')

  const handleClick = (value) => {
    if (value === '=') {
      try {
        setResult(eval(getValue))
      } catch (error) {
        setResult(error)
      }

    } else if (value === 'AC') {
      setValue('')
      setResult('')

    } else if (value === 'c') {
      setValue(getValue.slice(0, -1))
      setResult('')

    } else {
      setValue(getValue + value)

    }
  }
  return (
    <div>
        <h1 className='name'>Calculator</h1>
      <div className='Main-Container'>
        <div className='iconBox'>
          <span className='iconOne'>
            <i class="fa-solid fa-phone-volume"></i>
          </span>
          <span className='camera'></span>
          <span className='iconTwo'>
            <i class="fa-solid fa-wifi"></i>
            <i class="fa-solid fa-signal"></i>
            <i class="fa-solid fa-battery-half"></i>
          </span>
        </div>
        <input type="text" id="inputBox" value={getValue} readOnly />
        <input type='text' id='result' value={result} readOnly />
        <div className='Sec-Container'>
          <button className="btn AC clr" onClick={() => handleClick('AC')}>AC</button >
          <button className="btn clr" onClick={() => handleClick('-')}>-</button >
          <button className="btn clr" onClick={() => handleClick('/')}>÷</button >
          <button className="btn" onClick={() => handleClick('7')}>7</button >
          <button className="btn" onClick={() => handleClick('8')}>8</button >
          <button className="btn" onClick={() => handleClick('9')}>9</button >
          <button className="btn clr" onClick={() => handleClick('*')}>x</button >
          <button className="btn" onClick={() => handleClick('4')}>4</button >
          <button className="btn" onClick={() => handleClick('5')}>5</button >
          <button className="btn" onClick={() => handleClick('6')}>6</button >
          <button className="btn clr" onClick={() => handleClick('+')}>+</button >
          <button className="btn" onClick={() => handleClick('1')}>1</button >
          <button className="btn" onClick={() => handleClick('2')}>2</button >
          <button className="btn" onClick={() => handleClick('3')}>3</button >
          <button className="btn equal clr" onClick={() => handleClick('=')}>=</button >
          <button className="btn" onClick={() => handleClick('c')}>DEL</button >
          <button className="btn" onClick={() => handleClick('0')}>0</button >
          <button className="btn" onClick={() => handleClick('.')}>.</button >

        </div>
      </div>
    </div>
  )
}

export default MainPage
