import React from 'react'

function ListFile(props) {
    const name = ["shivam", "khushi", "satyam", "maa"]
    const countList = props.number.map(num => <h2>{num}</h2>)
    const namelist = name.map(names => <h2>{names}</h2>)
    return (
        <div>
            {countList}
            {namelist}
        </div>

    )

}

export default ListFile
