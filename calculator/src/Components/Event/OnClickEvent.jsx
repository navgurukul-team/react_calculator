import React from 'react'

function OnClick(props) {
  return (
    <div>
      <h1>This is onlick button</h1>
      <button onClick={()=>alert("working")}>Click me</button> 
      {/* It's basicly for how to use onclick in child components  */}

      <button onClick={()=>props.data()}>props button</button>
      {/* This is props onclick button  */}

      <button onClick={props.data}>props button 2</button>
      {/* Second way to use onclick button is without using arrow function you can able to use it. */}
    </div>
  )
}

export default OnClick
